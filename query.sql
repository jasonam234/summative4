show databases;
drop database shop;
create database shop;
use shop;

CREATE TABLE product(
	id int UNIQUE NOT NULL AUTO_INCREMENT,
    product_name varchar(50),
    category varchar(50),
    stock int,
    price long,
    created_at datetime,
    
    PRIMARY KEY(id)
);

CREATE TABLE transaction(
	id int UNIQUE NOT NULL AUTO_INCREMENT,
    product_id int,
    qty int,
    total_price int,
    created_at datetime,
    
    PRIMARY KEY(id),
    FOREIGN KEY(product_id) REFERENCES product(id) ON DELETE CASCADE
);

INSERT INTO product(product_name, category, stock, price, created_at) VALUES
("Product 1", "Category A", 1000, 10000, now()),
("Product 2", "Category A", 30, 20000, now());
-- ("Product 3", "Category B", 0, 1000, now()),
-- ("Product 4", "Category C", 100, 50000, now());

INSERT INTO transaction(product_id, qty, total_price, created_at) VALUES
(1, 5, 50000, now()),
(2, 5, 100000, now()),
(4, 5, 250000, now());

SELECT * FROM product;
SELECT * FROM transaction;

-- UPDATE product SET stock = stock - 5 WHERE id = 1;
 
-- SELECT * FROM transaction;

-- SELECT t.id, t.total_price, t.created_at, t.qty, p.id, p.product_name, p.category, p.stock, p.price, p.created_at FROM transaction t
-- JOIN product p ON p.id = t.product_id;

-- SELECT * FROM (SELECT * FROM product ORDER BY id DESC LIMIT 2) sub ORDER BY id ASC