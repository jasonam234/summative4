# summative4 - API Documentation

Simple Java-based shop keeping API. Build with Spring, with Spring Data JPA, MySQL Driver & Spring Web as the main dependency. 

Minimal unit testing & swagger documentation included in the code.



## Product Collection

| Method | Endpoint                                                 | Note                                      |
| ------ | -------------------------------------------------------- | ----------------------------------------- |
| GET    | /api/products                                            | Get all products present in database      |
| POST   | /api/product                                             | Insert new product entry                  |
| POST   | /api/product?stock=[int]&name=[string]&category=[string] | Search product by stock, name or category |
| PUT    | /api/product/{id}                                        | Update product entry by product ID        |
| DELETE | /api/product?id=[int]                                    | Delete product entry by product ID        |



### GET Method

#### Get all Products

**Endpoint**

```
/api/products
```



**200 response**

![get_product](.\images\get_product.PNG)

### POST Method

#### Insert new entry

**Endpoint**

```
/api/product
```



**Body**

![post_product_body](.\images\post_product_body.PNG)



**201 response**

![post_product_response](.\images\post_product_response.PNG)



**500 response**

Error occurred when item stock is 0.

![post_product_error](.\images\post_product_error.PNG)



#### Search product stock, name or category

**Endpoint**

```
/api/product?stock=[int]&name=[string]&category=[string]
```



**Example endpoint parameter**

Parameter optional.

```
/api/product?stock=5&name=Product 1&category=Category A
```



**200 response**

![post_search_product_response](.\images\post_search_product_response.PNG)



### PUT Method

#### Update product entry by ID

**Endpoint**

```
/api/product/{id}
```



**Example path variable**

```
/api/product/1
```



**Request body example**

![put_product_body](.\images\put_product_body.PNG)



**200 response**

![put_product_response](.\images\put_product_response.PNG)



**500 response**

Error occurred when product is not exist.

![put_product_error](.\images\put_product_error.PNG)



### DELETE Method

#### Delete product entry by ID

**Endpoint**

```
/api/product?id=[int]
```



**Example endpoint parameter**

```
/api/product?id=1
```



**200 response**

![delete_product_response](.\images\delete_product_response.PNG)



**500 response**

Error occurred when product is not exist.

![delete_product_error](.\images\delete_product_error.PNG)



## Transaction Collection

| Method | Endpoint            | Note                                 |
| ------ | ------------------- | ------------------------------------ |
| GET    | /api/payments       | Get all payments present in database |
| POST   | /api/payment        | Insert new payment entry             |
| POST   | /api/payment/search | Search payment by date               |



### GET Method

#### Get all payments present in database

**Endpoint**

```
/api/payments
```



**200 response**

![get_payment_response](.\images\get_payment_response.PNG)



### POST Method

#### Insert new payment entry

**Endpoint**

```
/api/payment
```



**Request body example**

![post_payment_body](.\images\post_payment_body.PNG)



**201 response**

![post_payment_response](.\images\post_payment_response.PNG)



**500 Response**

Error occurred if qty value more than stock.

![post_payment_error](.\images\post_payment_error.PNG)



#### Search payment by date

**Endpoint**

```
/api/payment/search
```



**Request body example**

![post_search_payment_body](.\images\post_search_payment_body.PNG)



**200 response**

![post_search_payment_response](.\images\post_search_payment_response.PNG)
