package com.jason.shop.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jason.shop.entity.Product;
import com.jason.shop.entity.Transaction;
import com.jason.shop.repository.ProductsRepository;
import com.jason.shop.repository.TransactionRepository;
import com.jason.shop.response.ProductResponse;
import com.jason.shop.response.ProductResponseError;
import com.jason.shop.response.ProductResponseObject;
import com.jason.shop.response.Response;
import com.jason.shop.response.TransactionResponse;
import com.jason.shop.response.TransactionResponseError;
import com.jason.shop.response.TransactionResponseObject;

@RestController
public class ShopController {
	@Autowired
	ProductsRepository productRepository;
	
	@Autowired
	TransactionRepository transactionRepository;
	
//	Product Collection
	@GetMapping(value = "/api/products", produces = "application/json")
	public ResponseEntity<Response> getAllProducts(){
		List<Product> data = productRepository.getAllProducts();
		Response response = new ProductResponse(HttpStatus.OK.value(), "GET all products operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PostMapping(value = "/api/product", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> postProduct(@RequestBody List<Product> payload){		
		for(int i = 0; i < payload.size(); i++) {
			if(payload.get(i).getCategory() == null || payload.get(i).getProduct_name() == null || payload.get(i).getStock() == 0) {
				Response response = new ProductResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "POST products operation terminated, null value in response body");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		}
		
		for(int i = 0; i < payload.size(); i++) {
			if(payload.get(i).getStock() == 0) {
				Response response = new ProductResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "POST products operation terminated, zero stock in item");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		}
		
		for(int i = 0; i < payload.size(); i++) {
			int data = productRepository.postProduct(payload.get(i).getProduct_name(), payload.get(i).getCategory(), payload.get(i).getStock(), payload.get(i).getPrice());
		}
		List<Product> responseData = productRepository.getLastInsertedProduct(payload.size());
		Response response = new ProductResponse(HttpStatus.CREATED.value(), "POST products operation succesful", responseData);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	
	@PutMapping(value = "/api/product/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> putProduct(@RequestBody Product payload, @PathVariable int id){
		if(payload.getCategory() == null || payload.getProduct_name() == null) {
			Response response = new ProductResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "PUT products operation terminated, null value in response body");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		int data = productRepository.putProduct(payload.getProduct_name(), payload.getCategory(), payload.getStock(), payload.getPrice(), id);

		if(data == 0) {
			Response response = new ProductResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "PUT product operation terminated, product not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		
		Product responseData = productRepository.getProductById(id);
		Response response = new ProductResponseObject(HttpStatus.OK.value(), "PUT product operation succesful", responseData);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@DeleteMapping(value = "/api/product", produces = "application/json")
	public ResponseEntity<Response> deleteProduct(@RequestParam int id){
		Product responseData = productRepository.getProductById(id);
		
		if(responseData == null) {
			Response response = new ProductResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "DELETE product operation terminated, product not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		
		int data = productRepository.deleteProduct(id);
		Response response = new ProductResponseObject(HttpStatus.OK.value(), "DELETE product operation succesful", responseData);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PostMapping(value = "api/product", produces = "application/json")
	public ResponseEntity<Response> searchProduct(@RequestParam(required = false) int stock, @RequestParam(required = false) String name, @RequestParam(required = false) String category){
		List<Product> responseData = productRepository.searchProduct(stock, name, category);
		Response response = new ProductResponse(HttpStatus.OK.value(), "POST search product operation succesful", responseData);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
//	Transaction Collection
	@GetMapping(value = "/api/payments", produces = "application/json")
	public ResponseEntity<Response> getAllTransactions(){
		List<Transaction> data = transactionRepository.getAllTransaction();
		Response response = new TransactionResponse(HttpStatus.OK.value(), "GET all transaction operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PostMapping(value = "/api/payment", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> postTransaction(@RequestBody Transaction payload){
		long totalPrice;
		
		Product productId = productRepository.getProductById(payload.getProduct().getId());
		if(productId == null) {
			Response response = new TransactionResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "POST transaction terminated, product not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}else if(payload.getQty() > productId.getStock()) {
			Response response = new TransactionResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "POST transaction terminated, quantity more than stock");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		
		totalPrice = payload.getQty() * productId.getPrice();
		
		int updateStock = productRepository.updateProductStock(payload.getQty(), productId.getId());
		int postTransaction = transactionRepository.postTransaction(payload.getProduct().getId(), payload.getQty(), totalPrice);
		
		Transaction responseData = transactionRepository.getLastTransaction();
		Response response = new TransactionResponseObject(HttpStatus.CREATED.value(), "POST transaction operation succesful", responseData);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	
	@PostMapping(value = "/api/payment/search", consumes = "application/json")
	public ResponseEntity<Response> searchTransactionByDate(@RequestBody Transaction payload){
		List<Transaction> responseData = transactionRepository.getTransactionByDate(payload.getCreated_at());
		Response response = new TransactionResponse(HttpStatus.OK.value(), "POST transaction search by date operation succesful", responseData);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
}
