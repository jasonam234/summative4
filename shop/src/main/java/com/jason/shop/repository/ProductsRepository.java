package com.jason.shop.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jason.shop.entity.Product;

@Transactional
public interface ProductsRepository extends JpaRepository<Product, Integer>{
	@Query(value = "SELECT * FROM product", nativeQuery = true)
	List<Product> getAllProducts();
	
	@Query(value = "SELECT * FROM product ORDER BY id DESC LIMIT :limit", nativeQuery = true)
	List<Product> getLastInsertedProduct(@Param("limit")int limit);
	
	@Query(value = "SELECT * FROM product WHERE (:stock is null or stock=:stock) and (:product_name is null or product_name=:product_name) and (:category is null or category=:category)", nativeQuery = true)
	List<Product> searchProduct(@Param("stock") int stock, @Param("product_name") String product_name, @Param("category") String category);
	
	@Query(value = "SELECT * FROM product WHERE id = ?1", nativeQuery = true)
	Product getProductById(int id);
	
	@Modifying
	@Query(value = "INSERT INTO product(product_name, category, stock, price, created_at) VALUES (?, ?, ?, ?, now())", nativeQuery = true)
	int postProduct(String product_name, String category, int stock, long price);
	
//	@Modifying
//	@Query(value = "UPDATE product SET (:product_name is null or product_name=:product_name) or (:category is null or category=:category), stock=:stock, price=:price WHERE id=:id", nativeQuery = true)
//	int putProduct(@Param("product_name") String product_name, @Param("category") String category, @Param("stock") long stock, @Param("price") long price, @Param("id") int id);
	
	@Modifying
	@Query(value = "UPDATE product SET product_name = ?1, category = ?2, stock = ?3, price = ?4 WHERE id = ?5", nativeQuery = true)
	int putProduct(String product_name, String category, long stock, long price, int id);
	
	@Modifying
	@Query(value = "UPDATE product SET stock = stock - ?1 WHERE id = ?2", nativeQuery = true)
	int updateProductStock(long stock, int id);
	
	@Modifying
	@Query (value = "DELETE FROM product WHERE id = ?1", nativeQuery = true)
	int deleteProduct(int id);
}
