package com.jason.shop.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.jason.shop.entity.Product;
import com.jason.shop.entity.Transaction;

@Transactional
public interface TransactionRepository extends JpaRepository<Transaction, Integer>{
	@Query(value = "SELECT * FROM transaction", nativeQuery = true)
	List<Transaction> getAllTransaction();
	
	@Query(value = "SELECT * FROM transaction WHERE id=(SELECT LAST_INSERT_ID())", nativeQuery = true)
	Transaction getLastTransaction();
	
	@Query(value = "SELECT * FROM transaction WHERE date(created_at) = ?1 ", nativeQuery = true)
	List<Transaction> getTransactionByDate(String date);
	
	@Modifying
	@Query(value = "INSERT INTO transaction(product_id, qty, total_price, created_at) VALUE(?, ?, ?, now())", nativeQuery = true)
	int postTransaction(int product_id, long qty, long total_price);
}
