package com.jason.shop.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	String product_name;
	String category;
	int stock;
	long price;
	String created_at;
	
	@OneToMany(mappedBy = "product")
	private List<Transaction> transaction = new ArrayList<>();
	
	public Product(int id, String product_name, String category, int stock, long price, String created_at) {
		super();
		this.id = id;
		this.product_name = product_name;
		this.category = category;
		this.stock = stock;
		this.price = price;
		this.created_at = created_at;
	}

	public Product(String product_name, String category, int stock, long price, String created_at) {
		super();
		this.product_name = product_name;
		this.category = category;
		this.stock = stock;
		this.price = price;
		this.created_at = created_at;
	}
	
	protected Product() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", product_name=" + product_name + ", category=" + category + ", stock=" + stock
				+ ", price=" + price + ", created_at=" + created_at + "]";
	}
}
