package com.jason.shop.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "transaction")
public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	
	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName="id")
	Product product;
	
	long qty;
	long total_price;
	String created_at;
	
	public Transaction(int id, Product product, long qty, long total_price, String created_at) {
		super();
		this.id = id;
		this.product = product;
		this.qty = qty;
		this.total_price = total_price;
		this.created_at = created_at;
	}

	public Transaction(Product product, long qty, long total_price, String created_at) {
		super();
		this.product = product;
		this.qty = qty;
		this.total_price = total_price;
		this.created_at = created_at;
	}

	protected Transaction(){}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public long getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public long getTotal_price() {
		return total_price;
	}

	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}
	
	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", product=" + product + ", qty=" + qty + ", total_price=" + total_price
				+ ", created_at=" + created_at + "]";
	}
}
