package com.jason.shop.response;

public class ProductResponseError implements Response{
	int status;
	String message;
	
	public ProductResponseError(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ProductResponseError [status=" + status + ", message=" + message + "]";
	}
}
