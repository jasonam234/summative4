package com.jason.shop;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLEngineResult.Status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jason.shop.controller.ShopController;
import com.jason.shop.entity.Product;
import com.jason.shop.entity.Transaction;
import com.jason.shop.response.ProductResponse;
import com.jason.shop.response.ProductResponseError;
import com.jason.shop.response.ProductResponseObject;
import com.jason.shop.response.Response;
import com.jason.shop.response.TransactionResponse;
import com.jason.shop.response.TransactionResponseError;
import com.jason.shop.response.TransactionResponseObject;

@WebMvcTest(ShopController.class)
public class ShopControllerTest {
	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	ObjectMapper mapper;
	
	@MockBean
	ShopController shopController;
	
	@Test
	public void getAllProductTest() throws Exception{
		Product productOne = new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54");
		Product productTwo = new Product(2, "BBB", "BBB", 20, 20000, "2021-10-29 15:14:54");
		Product productThree = new Product(3, "CCC", "CCC", 30, 30000, "2021-10-29 16:14:54");
		Product productFour = new Product(4, "DDD", "DDD", 40, 40000, "2021-10-29 17:14:54");
		
		List<Product> productList = new ArrayList<Product>();
		productList.add(productOne);
		productList.add(productTwo);
		productList.add(productThree);
		productList.add(productFour);
		
		Response response = new ProductResponse(HttpStatus.OK.value(), "GET all books operation succesful", productList);
		Mockito.when(shopController.getAllProducts()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(response));
	}
	
	@Test
	public void postProductTest() throws Exception{
		Product productOne = new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54");
		Product productTwo = new Product(2, "BBB", "BBB", 20, 20000, "2021-10-29 15:14:54");
		Product productThree = new Product(3, "CCC", "CCC", 30, 30000, "2021-10-29 16:14:54");
		Product productFour = new Product(4, "DDD", "DDD", 40, 40000, "2021-10-29 17:14:54");
		
		List<Product> productList = new ArrayList<Product>();
		productList.add(productOne);
		productList.add(productTwo);
		productList.add(productThree);
		productList.add(productFour);
		
		Response response = new ProductResponse(HttpStatus.CREATED.value(), "POST products operation succesful", productList);
		Mockito.when(shopController.postProduct(productList)).thenReturn(ResponseEntity.status(HttpStatus.CREATED).body(response));
	}
	
	@Test
	public void postProductTestError() throws Exception{
		Product productOne = new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54");
		Product productTwo = new Product(2, "BBB", "BBB", 20, 20000, "2021-10-29 15:14:54");
		Product productThree = new Product(3, "CCC", "CCC", 0, 30000, "2021-10-29 16:14:54");
		Product productFour = new Product(4, "DDD", "DDD", 40, 40000, "2021-10-29 17:14:54");
		
		List<Product> productList = new ArrayList<Product>();
		productList.add(productOne);
		productList.add(productTwo);
		productList.add(productThree);
		productList.add(productFour);
		
		Response response = new ProductResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "POST products operation terminated, zero stock in item");
		Mockito.when(shopController.postProduct(productList)).thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response));
	}
	
	@Test
	public void searchProductTest() throws Exception{
		Product productOne = new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54");
		Product productTwo = new Product(2, "AAA", "AAA", 30, 10000, "2022-10-29 14:14:54");
		
		List<Product> productList = new ArrayList<Product>();
		productList.add(productOne);
		productList.add(productTwo);
		Response response = new ProductResponse(HttpStatus.OK.value(), "POST search product operation succesful", productList);
		
		Mockito.when(shopController.searchProduct(10, "AAA", "AAA")).thenReturn(ResponseEntity.status(HttpStatus.OK).body(response));
	}
	
	@Test
	public void updateProductTest() throws Exception{
		Product productOne = new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54");
		
		Response response = new ProductResponseObject(HttpStatus.OK.value(), "PUT product operation succesful", productOne);
		Mockito.when(shopController.putProduct(productOne, 1)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(response));
	}
	
	@Test
	public void updateProductTestError() throws Exception{
		Product productOne = new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54");
		
		Response response = new ProductResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "PUT product operation terminated, product not found");
		Mockito.when(shopController.putProduct(productOne, 2)).thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response));
	}
	
	@Test
	public void deleteProductTest() throws Exception{
		Response response = new ProductResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "DELETE product operation terminated, product not found");
		Mockito.when(shopController.deleteProduct(999)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(response));
	}
	
	@Test
	public void deleteProductTestError() throws Exception{
		Product productOne = new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54");
		Response response = new ProductResponseObject(HttpStatus.OK.value(), "DELETE product operation succesful", productOne);
		Mockito.when(shopController.deleteProduct(10)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(response));
	}
	
	@Test
	public void getAllTransactionTest() throws Exception{
		Transaction transactionOne = new Transaction(new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54"), 5, 50000, "2021-10-29 15:14:54");
		Transaction transactionTwo = new Transaction(new Product(2, "BBB", "BBB", 20, 20000, "2021-10-29 15:14:54"), 5, 100000, "2021-10-29 16:14:54");
		Transaction transactionThree = new Transaction(new Product(3, "CCC", "CCC", 30, 30000, "2021-10-29 16:14:54"), 5, 150000, "2021-10-29 17:14:54");

		List<Transaction> transactionList = new ArrayList<Transaction>();
		transactionList.add(transactionOne);
		transactionList.add(transactionTwo);
		transactionList.add(transactionThree);
		
		Response response = new TransactionResponse(HttpStatus.OK.value(), "GET all books operation succesful", transactionList);
		Mockito.when(shopController.getAllTransactions()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(response));
	}
	
	@Test
	public void postTransactionTest() throws Exception{
		Transaction transactionOne = new Transaction(new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54"), 5, 50000, "2021-10-29 15:14:54");

		Response response = new TransactionResponseObject(HttpStatus.CREATED.value(), "POST transaction operation succesful", transactionOne);
		Mockito.when(shopController.postTransaction(transactionOne)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(response));
	}
	
	@Test
	public void postTransactionTestError() throws Exception{
		Transaction transactionOne = new Transaction(new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54"), 30, 50000, "2021-10-29 15:14:54");

		Response response = new TransactionResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "POST transaction terminated, quantity more than stock");
		Mockito.when(shopController.postTransaction(transactionOne)).thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response));
	}
	
	@Test
	public void searchTransactionByDateTest() throws Exception{
		Transaction transactionOne = new Transaction(new Product(1, "AAA", "AAA", 10, 10000, "2021-10-29 14:14:54"), 5, 50000, "2021-10-29 15:14:54");
		Transaction transactionTwo = new Transaction(new Product(2, "BBB", "BBB", 20, 20000, "2021-10-29 15:14:54"), 5, 100000, "2021-10-29 16:14:54");
		Transaction transactionThree = new Transaction(new Product(3, "CCC", "CCC", 30, 30000, "2021-10-29 16:14:54"), 5, 150000, "2021-10-29 17:14:54");

		List<Transaction> transactionList = new ArrayList<Transaction>();
		transactionList.add(transactionOne);
		transactionList.add(transactionTwo);
		transactionList.add(transactionThree);
		
		Response response = new TransactionResponse(HttpStatus.OK.value(), "POST transaction search by date operation succesful", transactionList);
		Mockito.when(shopController.searchTransactionByDate(transactionOne)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(response));
	}
}

